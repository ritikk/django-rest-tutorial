from django.shortcuts import render
from .models import Students
from .serializers import StudentSerialzer
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse


# Create your views here.
# For a student:
def student_detail(request, pk):
    stu = Students.objects.get(id=pk)
    serializer = StudentSerialzer(stu)
    json_data = JSONRenderer().render(serializer.data)
    return HttpResponse(json_data, content_type='application/json')


# For all students:
def all_student_detail(reqeust):
    stu = Students.objects.all()
    serializer = StudentSerialzer(stu, many=True)
    json_data = JSONRenderer().render(serializer.data)
    return HttpResponse(json_data, content_type='application/json')
