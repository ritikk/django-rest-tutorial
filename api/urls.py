from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('stuinfo/<int:pk>', views.student_detail),
    path('allstuinfo', views.all_student_detail),
]
