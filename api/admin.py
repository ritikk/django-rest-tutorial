from django.contrib import admin
from .models import Students

# Register your models here.
# admin.site.register(Students)

'''
The above and below both methods will have same output. Can use any one of the methods 
'''


@admin.register(Students)
class StudentAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'roll', 'city']
